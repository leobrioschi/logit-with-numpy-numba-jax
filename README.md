# Logistic Regression with Numpy/Numba/JAX

In this notebook I tried to naively optimize a classic **LR**(*MLE*) with *Gradient Descent* testing with Numpy, Numba and JAX. Please be aware that I am not an Applied Mathematician and tried to do things in the simplest of ways I could get from reading the docs.

I use data from Kaggle (*https://www.kaggle.com/datasets/blastchar/telco-customer-churn?resource=download*) and base my data cleaning over previous work (*https://gitlab.com/leobrioschi/modeling-customer-churn*).


## Results

Since I already vectorize everything that I can, I couldn't see much difference between **Numba** and **Numpy**, but I certainly didn't take any time trying to refactor my code or trying to make it play well with LLVM hooks. 

**JAX** on the other hand is *blazingly fast*(**lasers**) with not much beyond changing the linear algebra calls from **np.** to **jnp** and testing its' jit implementation over all the functions(*just skipped where it didn't work*).

**TL;DR -** Well, **JAX** is at least six times faster, even considering its' setup.

![Results from timed iterations](/web/results.png "Results from 10 consecutive runs")

Also, at the end of the day, there isn't much to optimize over this specific scenario

![Results from Logit](/web/cost_func.png "Cost function plot")

## About

I am Leonardo Brioschi, a *Ph.D. candidate at Accounting/Finance track*. Please visit **https://leobrioschi.gitlab.io**