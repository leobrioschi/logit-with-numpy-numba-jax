import numpy as np
from numba import jit
from numba import vectorize
import jax.numpy as jnp
from jax import jit as jjit



def np_sigmoid(z):
    result = np.divide(1,1 + np.exp(np.multiply(-1,z)))
    return result

def np_cost_lr(theta,X,y):
    m = len(y)
    const = 1/m
    hip = np_sigmoid(np.matmul(X,theta))
    min_y = -1 * y
    cost = const * sum(min_y * (np.log(hip)) -1* (1 + min_y) * (np.log(1 - hip)))
    #gradient = const * np.matmul(np.transpose(hip + min_y),X)
    gradient = const * np.matmul(np.transpose(X),(hip + min_y))
    return cost[0],gradient

def lr(theta,X,y,ite=500,alpha=0.0001,tolerance=1e-5):
    all_cost = list()
    for i in range(ite):
        cost,gradient = np_cost_lr(theta,X,y)
        if np.isnan(cost):
            theta = theta * 0.1
            cost,theta,all_cost2 = lr(theta,X,y,ite=(ite-i),alpha=alpha)
            all_cost = all_cost + all_cost2
            return cost,theta,all_cost
        if cost >= tolerance:
            all_cost.append(cost)
            theta = theta - (alpha*gradient)
        else:
            all_cost.append(cost)
            break
    return cost,theta,all_cost

@jit(nopython=True,fastmath=True,cache=True,nogil=True)
def nb_sigmoid(z):
    result = np.divide(1,1 + np.exp(np.multiply(-1,z)))
    return result

@jit(nopython=False,fastmath=True,cache=True,nogil=True)
def nb_cost_lr(theta,X,y):
    m = len(y)
    const = 1/m
    hip = nb_sigmoid(np.matmul(X,theta))
    min_y = -1 * y
    cost = const * sum(min_y * (np.log(hip)) -1* (1 + min_y) * (np.log(1 - hip)))
    #gradient = const * np.matmul(np.transpose(hip + min_y),X)
    gradient = const * np.matmul(np.transpose(X),(hip + min_y))
    return cost[0],gradient

@jit(nopython=False,fastmath=True,cache=True,nogil=True)
def nblr(theta,X,y,ite=500,alpha=0.0001,tolerance=1e-5):
    all_cost = list()
    for i in range(ite):
        cost,gradient = nb_cost_lr(theta,X,y)
        if np.isnan(cost):
            theta = theta * 0.1
            cost,theta,all_cost2 = nblr(theta,X,y,ite=(ite-i),alpha=alpha)
            all_cost = all_cost + all_cost2
            return cost,theta,all_cost
        if cost >= tolerance:
            all_cost.append(cost)
            theta = theta - (alpha*gradient)
        else:
            all_cost.append(cost)
            break
    return cost,theta,all_cost

@jjit
def jnp_sigmoid(z):
    result = jnp.divide(1,1 + jnp.exp(jnp.multiply(-1,z)))
    return result

@jjit
def jnp_cost_lr(theta,X,y):
    m = len(y)
    const = 1/m
    hip = jnp_sigmoid(jnp.matmul(X,theta))
    min_y = -1 * y
    cost = const * sum(min_y * (jnp.log(hip)) -1* (1 + min_y) * (jnp.log(1 - hip)))
    #gradient = const * np.matmul(np.transpose(hip + min_y),X)
    gradient = const * jnp.matmul(jnp.transpose(X),(hip + min_y))
    return cost[0],gradient


def jlr(theta,X,y,ite=500,alpha=0.0001,tolerance=1e-5):
    all_cost = list()
    for i in range(ite):
        cost,gradient = jnp_cost_lr(theta,X,y)
        if jnp.isnan(cost):
            theta = theta * 0.1
            cost,theta,all_cost2 = jlr(theta,X,y,ite=(ite-i),alpha=alpha)
            all_cost = all_cost + all_cost2
            return cost,theta,all_cost
        if cost >= tolerance:
            all_cost.append(cost)
            theta = theta - (alpha*gradient)
        else:
            all_cost.append(cost)
            break
    return cost,theta,all_cost