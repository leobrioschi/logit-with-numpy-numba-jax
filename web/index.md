# Logistic Regression with Numpy/Numba/JAX

In this notebook I tried to naively optimize a classic **LR**(*MLE*) with *Gradient Descent* testing with Numpy, Numba and JAX. Also, I didn't try **JAX** with GPU, which could make things even faster!

I use data from Kaggle (*https://www.kaggle.com/datasets/blastchar/telco-customer-churn?resource=download*)

**TL;DR -** Well, **JAX** is at least six times faster, even considering its' setup.

![Results from timed iterations](/web/results.png "Results from 10 consecutive runs")

## Author
I am Leonardo Brioschi, a Ph.D. Student at Fucape Business School, visit my site at [leobrioschi.gitlab.io](https://leobrioschi.gitlab.io)

## License
For this project, the UNLICENSE was the choice. Learn more in the repo.
